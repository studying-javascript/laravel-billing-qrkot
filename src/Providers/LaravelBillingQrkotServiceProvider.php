<?php

namespace Mostok\LaravelBillingQrkot\Providers;

use Illuminate\Support\ServiceProvider;
use Mostok\LaravelBillingQrkot\Console\Commands\CheckBillingStatusCommand;

class LaravelBillingQrkotServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');

            $this->publishes([
                __DIR__ . '/../../config/billing.php' => config_path('billing.php'),
            ]);

            $this->commands([
                CheckBillingStatusCommand::class,
            ]);
        }

        $this->loadRoutesFrom(__DIR__ . '/../../routes/api.php');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
    }
}