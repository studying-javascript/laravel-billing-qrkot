<?php

namespace Mostok\LaravelBillingQrkot\Console\Commands;

use Exception;
use Illuminate\Console\Command;
use Mostok\LaravelBillingQrkot\Models\Billing;
use Mostok\LaravelBillingQrkot\Services\BillingCheckService;

class CheckBillingStatusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:check-billing-status-command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     * @throws Exception
     */
    public function handle(BillingCheckService $service): void
    {
        Billing::query()->where('status', 'QRCDATA_CREATED')->get()->each($service);
    }
}
