<?php

namespace Mostok\LaravelBillingQrkot\Http\Controllers;

use App\Http\Controllers\Controller;
use Mostok\LaravelBillingQrkot\Models\UserCard;
use Mostok\LaravelBillingQrkot\Services\PaymentService;
use RuntimeException;

class UsersBillingController extends Controller
{

    private PaymentService $paymentService;
    public function __construct(PaymentService $paymentService) {
        $this->paymentService = $paymentService;
    }

    /**
     * @throws \Exception
     */
    public function addPayoutCard(): string {
        $user = auth()->user();
        $card = UserCard::where('user_id', $user->id)->first();
        if(isset($card) && isset($card->billing_data) && isset($card->billing_data->card_token)) {
            throw new RuntimeException('Карта уже была ранее добавлена', 403);
        }

        return $this->paymentService->getCardLink($user);
    }

    public function deletePayoutCard($id) {
        $user = auth()->user();
        $card = UserCard::where('id', $id)->where('user_id', $user->id)->delete();
        return true;
    }

}

