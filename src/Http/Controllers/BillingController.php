<?php

namespace Mostok\LaravelBillingQrkot\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JsonException;
use Mostok\LaravelBillingQrkot\Models\UserCard;

class BillingController extends Controller
{
    /**
     * @throws JsonException
     */
    public function userCallback(Request $request): void
    {
        $data = $request->all();
        if(isset($data['Order'], $data['Card'])) {
            $user = UserCard::find((int)str_replace("_user", "", $data['Order']['MerchantOrderId']));

            $billingData = !is_null($user->billing_data) ? $user->billing_data : null;
            if(
                isset($billingData->billing_id, $billingData->shop_id) &&
                $billingData->billing_id === $data['Order']['OrderId'] &&
                $billingData->shop_id === $data['Order']['ShopId'] &&
                ! isset($billingData->card_token) &&
                ! isset($billingData->card_mask)
            ) {
                $billingData->card_token = $data['Card']['Token'];
                $billingData->card_mask = $data['Card']['PanMask'];
                $user->update([
                    'billing_data' => $billingData
                ]);
            }
        }
    }
}
