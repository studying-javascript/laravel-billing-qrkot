<?php

namespace Mostok\LaravelBillingQrkot\Clients;

use Exception;
use GuzzleHttp\Client;

use RuntimeException;

class PaymentHttp
{
    private Client $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_url' => config('billing.api_url'),
            'headers' => ['x-api-key' => config('billing.x-api-key')],
            'auth' => [config('billing.login'), config('billing.password')]
        ]);
    }

    private function checkErrorResponse(array $data): void
    {
        if(!$data['Response']['Success']) {
            throw new RuntimeException('Произошла ошибка со стороны сервиса оплаты, повторите попытку позже.', 400);
        }
    }

    /**
     * @throws Exception
     */
    public function createOrder(array $data): ?array
    {
        $response = json_decode($this->client
            ->post('payments/create', ['body' => json_encode($data, JSON_THROW_ON_ERROR)])
            ->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);

        $this->checkErrorResponse($response);

        return $response['Order'];
    }

    /**
     * @throws Exception
     */
    public function payout(array $data): ?array
    {
        $response = json_decode($this->client
            ->post('payments/cards/payout', ['body' => json_encode($data, JSON_THROW_ON_ERROR)])
            ->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);

        $this->checkErrorResponse($response);

        return $response;
    }

    /**
     * @throws Exception
     */
    public function status($orderId): ?array
    {
        $response = json_decode($this->client
            ->post('payments/get', [
                'body' => json_encode([
                    'OrderId' => $orderId,
                ], JSON_THROW_ON_ERROR)
            ])
            ->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);

        $this->checkErrorResponse($response);

        return $response['Order'];
    }

    /**
     * @throws Exception
     */
    public function qrcData($orderId): ?array
    {
        $response = json_decode($this->client
            ->post('payments/ips/qrcData', [
                'body' => json_encode([
                    'OrderId' => $orderId,
                    'QrcType' => '02',
                    "TemplateVersion" => "01",
                    "QrTtl" => "60"
                ], JSON_THROW_ON_ERROR)
            ])
            ->getBody()->getContents(), false, 512, JSON_THROW_ON_ERROR);

        $this->checkErrorResponse($response);

        return $response;
    }

    /**
     * @throws Exception
     */
    public function refund($orderId): ?array
    {
        $response = json_decode($this->client
            ->post('payments/refund', ['body' => json_encode([
                'OrderId' => $orderId,
            ], JSON_THROW_ON_ERROR)
            ])
            ->getBody()->getContents(), false, 512, JSON_THROW_ON_ERROR);

        $this->checkErrorResponse($response);

        return $response['Refund'];
    }

}
