<?php

namespace Mostok\LaravelBillingQrkot\Services;

use App\Models\Billing;
use App\Models\Order;
use App\States\Order\ExpectCompleted;

class BillingCheckService
{

    public function __construct(private PaymentService $paymentService) {}

    public function __invoke(Billing $item): void
    {
        $this->paymentService->status($item);
        $item->refresh();
        if($item->status === 'EXPIRED') {
            $item->delete();
        } else if ($item->status === 'IPS_ACCEPTED') {
            Order::where('id', $item->order_id)->first()->status->transitionTo(ExpectCompleted::class);
        }
    }

}
