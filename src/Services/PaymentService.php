<?php

namespace Mostok\LaravelBillingQrkot\Services;


use App\Models\User;
use Exception;
use Mostok\LaravelBillingQrkot\Clients\PaymentHttp;
use Mostok\LaravelBillingQrkot\Models\Billing;
use Mostok\LaravelBillingQrkot\Models\UserCard;

class PaymentService
{
    private PaymentHttp $payment;
    public function __construct(PaymentHttp $payment){
        $this->payment = $payment;
    }

    /**
     * @throws Exception
     */
    public function payIn($merchantOrderId, $amount): Billing
    {
        $billing = Billing::where('order_id', $merchantOrderId)->where("type", "PayIn")->first();

        if($billing) {
            $checkService = app(BillingCheckService::class);
            $checkService($billing);
        }

        if(!$billing || $billing->deleted_at) {
            $paymentData = [
                "MerchantOrderId" => $merchantOrderId. "_in",
                "Currency" => "RUB",
                "Type" => "PayIn",
                "PaymentTypes" => ['IPS'],
                "Amount" => (int) ($amount * 100),
            ];

            $data = $this->payment->createOrder($paymentData);
            $billing = Billing::create([
                'billing_id' => $data['OrderId'],
                'type' => 'PayIn',
                'status' => $data['Status'],
                'order_id' => $merchantOrderId,
                'amount' => $data['Amount'],
                'user_id' => auth()->user()->id,
                'safe_deal_id' => $data['ParentOrder']['Id'],
            ]);

            $qrData = $this->payment->qrcData($billing->billing_id);
            $billing->update([
                'status' => $qrData['Order']['Status'],
                'qrc' => $qrData['Qrc']
            ]);
        }

        return $billing;
    }

    /**
     * @throws Exception
     */
    public function payOut(int $merchantOrderId, string $cardToken): bool
    {
        $billingPayIn = Billing::where('order_id', $merchantOrderId)->where("type", "PayIn")->first();
        $billing = Billing::where('order_id', $merchantOrderId)->where("type", "PayOut")->first();
        if(!$billing) {
            $paymentData = [
                "MerchantOrderId" => $merchantOrderId. "_out",
                "Currency" => "RUB",
                "Type" => "PayOut",
                "PaymentTypes" => ['CARD'],
                "Amount" => (int) $billingPayIn->amount,
                "ParentOrder" => [
                    "Id" => $billingPayIn->safe_deal_id,
                    "Type" => "SafeDeal"
                ]
            ];

            $data = $this->payment->createOrder($paymentData);
            $billing = Billing::create([
                'billing_id' => $data['OrderId'],
                'type' => 'PayOut',
                'status' => $data['Status'],
                'order_id' => $merchantOrderId,
                'amount' => $data['Amount'],
                'user_id' => auth()->user()->id,
                'safe_deal_id' => $data['ParentOrder']['Id'],
            ]);
        }

        $payoutData = [
            'OrderId' => $billing->billing_id,
            'Receiver' => [
                'Token' => $cardToken
            ]
        ];
        $data = $this->payment->payout($payoutData);
        $billing->update([
            'status' => $data['Order']['Status'],
        ]);
        return true;
    }

    /**
     * @throws Exception
     */
    public function refund(int $merchantOrderId): void
    {
        $billing = Billing::where('order_id', $merchantOrderId)->where("type", "PayIn")->first();

        if(!$billing) {
            throw new \RuntimeException('Вы не можете вывести средства у не оплаченного заказа', 400);
        }

        $payoutData = [
            'OrderId' => $billing->billing_id,
        ];
        $response = $this->payment->payout($payoutData);

        $billing->update([
            'status' => $response['Order']['Status'],
        ]);
    }

    /**
     * @throws Exception
     */
    public function status(Billing $billing): void
    {
        $response = $this->payment->status($billing->billing_id);

        $billing->update(['status' => $response['Status']]);
    }

    /**
     * @throws Exception
     */
    public function getCardLink(User $user): string
    {
        $billingData = UserCard::where('user_id', $user->id)->first()->billing_data ?? null;

        if(!$billingData || (!$billingData->billing_id && !$billingData->shop_id)) {
            $paymentData = [
                "MerchantOrderId" => $user->id. "_user",
                "Currency" => "RUB",
                "Type" => "PayIn",
                "PaymentTypes" => ['CARD'],
                "Amount" => 0,
                'CallbackUrl' => route('billing.callback')
            ];

            $data = $this->payment->createOrder($paymentData);

            $billingData = UserCard::updateOrCreate(['user_id' => $user->id], json_decode(json_encode([
                'shop_id' => $data['ShopId'],
                'billing_id' => $data['OrderId'],
            ], JSON_THROW_ON_ERROR), false, 512, JSON_THROW_ON_ERROR));
        }

        if($billingData->billing_id && $billingData->shop_id) {
            return config('services.billing.form_url') . '?' .http_build_query(['ShopId' => $billingData->shop_id, 'OrderId' => $billingData->billing_id]);
        }

        throw new \RuntimeException('Неизвестная ошибка', 400);
    }
}

