<?php

use Illuminate\Routing\Route;
use Mostok\LaravelBillingQrkot\Http\Controllers\BillingController;

Route::post('billing/user-callback', [BillingController::class, 'userCallback'])->name('billing.callback');

