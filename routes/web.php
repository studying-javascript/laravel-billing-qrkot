<?php

use Illuminate\Routing\Route;
use Mostok\LaravelBillingQrkot\Http\Controllers\UsersBillingController;

Route::get('user/add-card', [UsersBillingController::class, 'addPayoutCard']);
Route::get('user/delete-card', [UsersBillingController::class, 'deletePayoutCard']);
