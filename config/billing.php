<?php

return [
    'api_url' => env('BILLING_API_URL'),
    'form_url' => env('BILLING_FORM_URL'),
    'x-api-key' => env('BILLING_API_KEY'),
    'login' => env('BILLING_LOGIN'),
    'password' => env('BILLING_PASSWORD'),
];

